export interface Employee {

    name: string;
    address:string;
    dob:string;
    role:string;
    phone:Number;
    gender:string;

}