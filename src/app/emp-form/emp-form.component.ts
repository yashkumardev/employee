import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormsModule} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Employee } from 'src/assets/employeeInterface';

@Component({
  selector: 'app-emp-form',
  templateUrl: './emp-form.component.html',
  styleUrls: ['./emp-form.component.css']
})
export class EmpFormComponent implements OnInit {
  today=new Date;
  employeeForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder,private toastrService: ToastrService) { }
  genders=['Male','Female'];
  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      dob: ['', Validators.required],
      role: ['', [Validators.required]],
      phone:  ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]] ,
      gender: ['Male', Validators.required]
  });
  }
  get f() { return this.employeeForm.controls; }

  onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.employeeForm.invalid) {
          return;
      }
      else{
        let employeeList:Employee[]=[];
        let employee:Employee=
        {
          name:this.employeeForm.controls['name'].value,
          address:this.employeeForm.controls['address'].value,
          dob:this.employeeForm.controls['dob'].value,
          role:this.employeeForm.controls['role'].value,
          phone:this.employeeForm.controls['phone'].value,
          gender:this.employeeForm.controls['gender'].value
        }
        if(sessionStorage.getItem('employeeList'))
        {
          employeeList=JSON.parse(sessionStorage.getItem('employeeList'));
        }
        employeeList.push(employee);
        sessionStorage.setItem('employeeList',JSON.stringify(employeeList))
        this.toastrService.success('Added Successfully');
        this.employeeForm.reset();
        this.submitted=false;
      }

  }

  onReset() {
      this.submitted = false;
      this.employeeForm.reset();
  }
}
