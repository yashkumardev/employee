import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmpFormComponent } from './emp-form.component';
import { RouterModule } from '@angular/router';

const routerConfig=[
  {path:'',component:EmpFormComponent}
]
@NgModule({
  declarations: [EmpFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routerConfig),FormsModule,
  ],
  providers:[DatePipe]
})
export class EmpFormModule { }
