import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EmpListComponent } from './emp-list.component';

const routerConfig=[
  {path:'',component:EmpListComponent}
]
@NgModule({
  declarations: [EmpListComponent],
  imports: [
    CommonModule,RouterModule.forChild(routerConfig)
  ]
})
export class EmpListModule { }
