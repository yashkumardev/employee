import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/assets/employeeInterface';
 
interface Student {
  id: Number;
  name: String;
  email: String;
  gender: String;
}

@Component({
  selector: 'app-emp-list',
  templateUrl: './emp-list.component.html',
  styleUrls: ['./emp-list.component.css']
})
export class EmpListComponent implements OnInit {
   employeeList:Employee[];

  constructor() { }

  ngOnInit() {
    this.getEmployees();
  }
  getEmployees()
  {
    if(sessionStorage.getItem('employeeList'))
    {
      this.employeeList=JSON.parse(sessionStorage.getItem('employeeList'));
    }
  }
  
}
