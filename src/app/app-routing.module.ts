import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'',loadChildren:()=> import('./emp-form/emp-form.module').then(m=>m.EmpFormModule)},
  {path:'list',loadChildren:()=> import('./emp-list/emp-list.module').then(m=>m.EmpListModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
